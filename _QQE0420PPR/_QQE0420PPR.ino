/*
  QQE04-20PP Right
 Version 2.0
 */
#include <SimpleTimer.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>

static const byte QQE0420PPL_ID = 33;
static const byte ampId = QQE0420PPL_ID;

// Pin Config
#define relayPin		  3
#define ledOnBoardPin		  4
#define reg1Pin		          9
#define reg2Pin		          10
#define ledPin		          13
#define current1Pin	          A0    
#define current2Pin	          A1 

// Constants
#define startCurrent              30       // 12.45/mA
#define minCurrent                250        
#define maxCurrent                750       
#define nominalCurrent            498 
#define dischargeMaxTime	  5   	   // seconds
#define heatMaxTime	          35   	   // seconds
#define highVoltageMaxTime	  2   	   // seconds
#define regulationMaxTime	  10   	   // seconds
#define stabilizationMaxTime	  300      // seconds
#define reRegulationMinTime	  5000     // milli-seconds
#define masterP                   3          
#define masterI                   0.00005            
#define masterD                   0      
#define slaveP                    2        
#define slaveI                    0.00005         
#define slaveD                    0
#define pidSampleTime             10 
#define switchOffset		  122      // 0.0049V per Units (0.6V)
#define regulationTreshold        1
#define stabilizedTreshold        60 
#define treshold                  10          
#define currentInputAverageRatio  100 
#define pidSetPointSlaveRatio     10 

// Internal use
SimpleTimer sendTimer;
Blink led;
Blink ledOnBoard;
double current1Average = 0;
double current2Average = 0;
double pid1Output;                      //Initialized on resetRegulators()
double pid2Output;                      //Initialized on resetRegulators() 
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned long dischargeStartTime;        //Initialized on reset()
unsigned long heatStartTime;             //Initialized on reset()
unsigned long highVoltageStartTime;      //Initialized on reset()
unsigned long regulationStartTime;       //Initialized on reset()
unsigned long functionStartTime;         //Initialized on reset()
unsigned long reRegulationStartTime;     //Initialized on reset()
byte percentageSetPoint;                 //Initialized on reset()
double pidSetPoint;                      //Initialized on reset()
double pid2SetPoint;                     //Initialized on reset()  
boolean stabilized = false;

// Init regulators
PID pid1(&current1Average, &pid1Output, &pidSetPoint, masterP, masterI, masterD, 0, 255, pidSampleTime, false);
PID pid2(&current2Average, &pid2Output, &pid2SetPoint, slaveP, slaveI, slaveD, 0, 255, pidSampleTime, false);

// Sequence:
#define SEQ_DISCHARGE              0      // 0: Discharge
#define SEQ_HEAT                   1      // 1: Heat tempo 
#define SEQ_STARTING               2      // 2: Starting High Voltage
#define SEQ_REGULATING             3      // 3: Waiting for reg
#define SEQ_FUNCTION               4      // 4: Normal Fonction
#define SEQ_FAIL                   5      // 5: Fail
int sequence = SEQ_DISCHARGE;

// Errors
#define NO_ERR                       0      // No error
#define ERR_DISHARGETOOLONG          2      // 2: Discharge too long
#define ERR_CURRENTONHEAT            3      // 3: Current during heat time
#define ERR_SATRTINGOUTOFRANGE       4      // 4: Out of range during starting
#define ERR_REGULATINGTOOLONG        5      // 5: Stabilization too long
#define ERR_REGULATINGMAXREACHED     6      // 6: maximun reached during regulation
#define ERR_REGULATINGMINREACHED     7      // 7: minimum reached during regulation
#define ERR_FUNCTIONMAXREACHED       8      // 8: maximun reached during normal function
#define ERR_FUNCTIONMINREACHED       9      // 9: minimum reached during normal function
#define ERR_STARTINGTOOLONG          10     // 10: Starting too long
byte errorNumber = NO_ERR;

#define ERR_TUBE_1       1
#define ERR_TUBE_2       2
byte errorCause = NO_ERR;

#define CHECK_RANGE_OK       0
#define CHECK_RANGE_TOOLOW   1
#define CHECK_RANGE_TOOHIGH  2

// Diagnostic
EasyTransfer dataTx; 
dataResponse dataTxStruct;

void reset()
{
  dischargeStartTime = 0;
  heatStartTime = 0;
  highVoltageStartTime = 0;
  regulationStartTime = 0;
  reRegulationStartTime = 0;
  functionStartTime = 0;
  percentageSetPoint = 0;
  pidSetPoint = nominalCurrent;
  pid2SetPoint = 0;
  resetRegulators();
}

void sendDatas()
{    
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(current1Average, 0, 1023, 0, 255);   // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(current2Average, 0, 1023, 0, 255);
  dataTxStruct.output0 = constrain((int)pid1Output, 0, 255);
  dataTxStruct.output1 = constrain((int)pid2Output, 0, 255);
  dataTxStruct.minValue = map(minCurrent, 0, 1023, 0, 255);
  dataTxStruct.refValue = map(pidSetPoint, 0, 1023, 0, 255); 
  dataTxStruct.maxValue = map(maxCurrent, 0, 1023, 0, 255);  
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorCause;
  dataTx.sendData();
}

void relayOn(void) 
{
  analogWrite(relayPin, 128);
}

void relayOff(void) 
{
  analogWrite(relayPin, 0);
}

void resetRegulators(){
  relayOff(); 

  pid1.SetEnabled(false);  
  pid2.SetEnabled(false);  

  pid1Output = 0;
  pid2Output = 0;

  analogWrite(reg1Pin, 0);
  analogWrite(reg2Pin, 0);
}

void initRegulators()
{
  pid1.SetEnabled(true);
  pid2.SetEnabled(true);
}

void computeRegulators()
{
  if (pid1.Compute()) {
    analogWrite(reg1Pin, constrain((int)pid1Output, 0, 255));
  }
  if (pid2.Compute()) {
    analogWrite(reg2Pin, constrain((int)pid2Output, 0, 255));
  }
}

unsigned int checkInRange(double minValue, double maxValue)
{
  if (minValue > 0){
    if (current1Average < minValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOLOW;
    } 
    if (current2Average < minValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0){
    if (current1Average > maxValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOHIGH;
    } 
    if (current2Average > maxValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERR;
  return CHECK_RANGE_OK;
}

unsigned int calcRegulationProgress(double minValue, double maxValue, double range)
{
  double percentProgress = 100;

  if (current1Average < minValue)
  {
    percentProgress = 100 * (1 - (minValue - current1Average) / range);
  }
  else if (current1Average > maxValue)
  {
    percentProgress = 100 * (1 - (current1Average - maxValue) / range);
  }

  if (current2Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current2Average) / range), percentProgress);
  }
  else if (current2Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current2Average - maxValue) / range), percentProgress);
  }

  return constrain((int)percentProgress, 0, 100);
}

void regulate(){
  functionStartTime = 0;
  sequence = SEQ_REGULATING;
}

// the setup routine runs once when you press reset:
void setup() {                
  led.Setup(ledPin, false);
  ledOnBoard.Setup(ledOnBoardPin, false);
  sendTimer.setInterval(200, sendDatas);

  // Diagnostic
  Serial.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);

  reset();
}

// the loop routine runs over and over again forever:
void loop() 
{ 
  unsigned int currentTime;
  unsigned int check;

  if (sequence != SEQ_FAIL){
    // Read and smooth the input
    current1Average += (analogRead(current1Pin)-current1Average)/currentInputAverageRatio;
    current2Average += (analogRead(current2Pin)-current2Average)/currentInputAverageRatio;

    // Average set points for slave regulators from master measure
    pid2SetPoint += (current1Average-pid2SetPoint)/pidSetPointSlaveRatio; 
  }

  // Diagnostic
  sendTimer.run();

  switch (sequence)
  {  
  case SEQ_DISCHARGE:	
    // Discharging

    // Reset errors
    errorCause = NO_ERR;
    errorNumber = NO_ERR;

    // Pre-sequence
    if (dischargeStartTime == 0){
      dischargeStartTime = millis();
      resetRegulators();
    }

    led.Execute(800, 200);
    ledOnBoard.Execute(800, 200);

    // Calc elapsed time in seconds
    currentTime = (millis() - dischargeStartTime) / 1000;

    // Diagnostic
    stepMaxTime = dischargeMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = 0;
    stepCurValue = 1;

    if(currentTime > dischargeMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_DISHARGETOOLONG;
      break;
    }

    if(checkInRange(0, startCurrent) == CHECK_RANGE_TOOHIGH)
    {
      break;
    }

    // Post-sequence
    dischargeStartTime = 0;
    sequence++;

  case SEQ_HEAT: 
    // Startup tempo 

    // Pre-sequence
    if (heatStartTime == 0){
      heatStartTime = millis();
      resetRegulators();
    }

    led.Execute(400, 400);
    ledOnBoard.Execute(400, 400);

    // Calc elapsed time in seconds
    currentTime = (millis() - heatStartTime) / 1000;

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = currentTime;

    // Ensure no current at this step
    if(checkInRange(0, startCurrent + 10) == CHECK_RANGE_TOOHIGH)
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }

    if(currentTime < heatMaxTime)
    {
      break;
    }

    // Diagnostic, force 100%
    stepElapsedTime = heatMaxTime;
    stepCurValue = heatMaxTime;

    // Post-sequence
    heatStartTime = 0;
    led.On();
    ledOnBoard.On();
    sequence++;
    delay(500);   

  case SEQ_STARTING:
    // Starting High Voltage

    // Pre-sequence
    if (highVoltageStartTime == 0){
      highVoltageStartTime = millis();

      // Ensure relay and led
      relayOn();
      led.On();
      ledOnBoard.On();
    }

    // Calc elapsed time in seconds
    currentTime = (millis() - highVoltageStartTime) / 1000;

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = highVoltageMaxTime;
    stepCurValue = currentTime;

    if(currentTime < highVoltageMaxTime)
    {
      break;  
    }  

    // Post-sequence
    heatStartTime = 0;
    sequence++;

  case SEQ_REGULATING: 
    // Waiting for reg       

    // Pre-sequence
    if (regulationStartTime == 0){
      regulationStartTime = millis();      
      initRegulators();
      relayOn();
    }

    // Regulation
    computeRegulators(); 

    // Ensure relay, atx and led
    led.Execute(20, stabilized ? 1500 : 500);    
    ledOnBoard.Execute(20, stabilized ? 1500 : 500);    

    // Calc elapsed time in seconds
    currentTime = (millis() - regulationStartTime) / 1000;

    // Diagnostic
    stepMaxTime = stabilized ? stabilizationMaxTime : regulationMaxTime;
    stepElapsedTime = currentTime;
    stepMaxValue = 100;
    if (stabilized) {
      stepCurValue = calcRegulationProgress(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold, stabilizedTreshold - regulationTreshold); 
    } 
    else {
      stepCurValue = calcRegulationProgress(pidSetPoint - stabilizedTreshold, pidSetPoint + stabilizedTreshold, pidSetPoint - stabilizedTreshold); 
    }

    if(currentTime > (stabilized ? stabilizationMaxTime : regulationMaxTime))
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGTOOLONG;
      break;
    }

    check = checkInRange(stabilized ? minCurrent : startCurrent, maxCurrent);
    if (check != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_REGULATINGMINREACHED : ERR_REGULATINGMAXREACHED;
      break;      
    }

    if(!stabilized && checkInRange(pidSetPoint - stabilizedTreshold, pidSetPoint + stabilizedTreshold) == CHECK_RANGE_OK)
    {
      stabilized = true;
      regulationStartTime = millis();   
    }

    // If target points not reached, continue to regulate
    if(checkInRange(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    regulationStartTime = 0;

    sequence++;

  case SEQ_FUNCTION:
    // Normal Fonction, wait and see        

    // Pre-sequence
    if (functionStartTime == 0){
      functionStartTime = millis();

      // Ensure relay, atx and led
      relayOn();
      led.Off();
      ledOnBoard.Off();
    }

    // Calc elapsed time in minuts
    currentTime = (millis() - functionStartTime) / 60000; 

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = currentTime;
    stepMaxValue = 0;
    stepCurValue = 0;

    check = checkInRange(minCurrent, maxCurrent);
    if(check != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
      break;
    }

    if(checkInRange(pidSetPoint - treshold, pidSetPoint + treshold) != CHECK_RANGE_OK)
    {
      if (reRegulationStartTime == 0){
        reRegulationStartTime = millis();
      }

      // If fail times elapsed, regulate again
      if (millis() - reRegulationStartTime > reRegulationMinTime)
      {    
        // Regulate again
        reRegulationStartTime = 0;
        regulate();
        break;
      }          
    }
    else
    {
      reRegulationStartTime = 0;
    }
    break;

  default: 
    // Fail, protect mode
    // Ensure relay, and atx off
    reset();

    // Error indicator
    // Otherwise display error number or tube number
    led.Execute(250, errorNumber, 1200);
    ledOnBoard.Execute(250, errorNumber, 1200);
  }  
}


